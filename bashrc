# Basics
[[ $- != *i* ]] && return		#bash does nothing when not running interactively

# set +o history			#turn history off
set -o vi				#set vi mode as default mode for GNU readline
export VISUAL="vim"                     #set vim as default editor in nnn
export EDITOR="vim" 			#set default editor as vim
export LESSHISTFILE="-"                 #enforce XDG base directory standards
export LANG=en_IN.UTF-8			#set locale as UTF-8 compatible

# Alias
# {
#  Utils
alias e="exit"
alias c="clear"
alias rm="rm -iv"
alias mv="mv -iv"

#  Programs
alias v="vim"
alias t="tmux"
alias di="sdcv"
alias p="pandoc"
alias co="curl -O"
alias yt="youtube-dl"
alias up="doas pacman -Syu"
alias unmod="doas modprobe xhci_pci"
alias mod="doas modprobe -r xhci_pci"
alias pw="pandoc -f html -t markdown"
alias arec="ffmpeg -f alsa -i hw:0 out.opus"
alias vrec="ffmpeg -f x11grab -s 1920x1080 -i :0.0 -f alsa -i hw:0 out.mkv"

#  URL shortner
alias qnd="read -p \"Enter URL:\" && curl -X POST -d \"$REPLY\" https://qnd.be"

# Ephemeral file hosting at https://oshi.at
alias oshi="read -p \"Enter file path\" && curl -T \"$REPLY\" https://oshi.at"

#  USB mounting
#  N.B: This assumes the usb is always mounted at sdb1
#  Also: mkdir ~/Downloads/mount/ in case it does not already exist
alias u1="lsblk"
alias u2="doas mount /dev/sdb1 ~/down/mount/"
alias u3="doas umount ~/down/mount/"

#  Mobile mounting and unmounting
alias s1="simple-mtpfs -l"
alias s2="simple-mtpfs --device 1 ~/down/mount"
alias s3="fusermount -u ~/down/mount"

#  Get one-line weather report
# alias wttr="curl https://wttr.in/?format=3"
# }

# Set $PS1 prompt
PS1="\[\033[01;31m\][\W]\033[01;31m\] » \[\033[00m\]"
