set number
syntax on
colo inkpot
set fillchars+=vert:!
set complete+=kspell
autocmd bufreadpre *.md set textwidth=60
autocmd bufreadpre *.txt set textwidth=60
autocmd BufRead,BufNewFile *.md set spell spelllang=en_gb
autocmd BufRead,BufNewFile *.gmi set spell spelllang=en_gb
autocmd BufRead,BufNewFile *.txt set spell spelllang=en_gb
