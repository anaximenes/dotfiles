--------------------------------------------------------
--   Persephone - a theme for awesome window manager  --
--                                                    --
--           Copyright (C) 2020 Anaximenes            --
--                                                    --
--             Licence: GPL v3.0 or later             --
--------------------------------------------------------

 
--  This theme file is free software; you can redistribute it and/or modify it under the terms of the GNU Library General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public License for more details. You should have received a copy of the GNU Library General Public License along with this library; if not, see <http://www.gnu.org/licenses/>. --


local themes_path = require("gears.filesystem").get_themes_dir()
local theme_assets = require("beautiful.theme_assets")
local dpi = require("beautiful.xresources").apply_dpi

-- {{{ Main
local theme = {}
theme.wallpaper = "/home/user/.config/awesome/wall.jpg"
-- }}}

-- {{{ Styles
theme.font      = "Monospace Bold 20"

-- {{{ Colors
theme.fg_normal = "#F7F7F7"
theme.fg_focus = "#D21242"
theme.fg_urgent = "#000000"
theme.fg_minimize = "#FFFFFF"
theme.bg_normal = "#171717"
theme.bg_focus = "#2F2F2F"
theme.bg_urgent = "#FFFFFF"
theme.bg_minimize = "#444444"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.useless_gap   = dpi(4)
theme.border_width  = dpi(2)
theme.border_normal = "#C5C5C5"
theme.border_focus = "#D21242"
theme.border_marked = "#CC9393"
-- }}}


-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor] --
-- }}} --

-- {{{ Menu --
-- Variables set for theming the menu: --
-- menu_[bg|fg]_[normal|focus] --
-- menu_[border_color|border_width] --
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)
-- }}} --

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#222222"
theme.titlebar_bg_normal = "#4a4e4c"
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = themes_path .. "zenburn/taglist/squarefz.png"
theme.taglist_squares_unsel = themes_path .. "zenburn/taglist/squarez.png"
--theme.taglist_squares_resize = "false" --
-- }}} --

-- {{{ Misc
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, c1, c2)
theme.menu_submenu_icon      = themes_path .. "default/submenu.png"
-- }}}

-- {{{ Custom colour for icon design --
-- c1 = "#F8F4FF" --
-- c2 = "#000000" --
-- }}} --

-- {{{ Layout
theme.layout_tile       = themes_path .. "zenburn/layouts/tile.png"
theme.layout_max        = themes_path .. "zenburn/layouts/max.png"
theme.layout_floating   = themes_path .. "zenburn/layouts/floating.png"
-- theme.layout_tileleft   = themes_path .. "zenburn/layouts/tileleft.png" --
-- theme.layout_tilebottom = themes_path .. "zenburn/layouts/tilebottom.png" --
-- theme.layout_tiletop    = themes_path .. "zenburn/layouts/tiletop.png" --
-- theme.layout_fairv      = themes_path .. "zenburn/layouts/fairv.png" --
-- theme.layout_fairh      = themes_path .. "zenburn/layouts/fairh.png" --
-- theme.layout_spiral     = themes_path .. "zenburn/layouts/spiral.png" --
-- theme.layout_dwindle    = themes_path .. "zenburn/layouts/dwindle.png" --
-- theme.layout_fullscreen = themes_path .. "zenburn/layouts/fullscreen.png" --
-- theme.layout_magnifier  = themes_path .. "zenburn/layouts/magnifier.png" --
-- theme.layout_cornernw   = themes_path .. "zenburn/layouts/cornernw.png" --
-- theme.layout_cornerne   = themes_path .. "zenburn/layouts/cornerne.png" --
-- theme.layout_cornersw   = themes_path .. "zenburn/layouts/cornersw.png" --
-- theme.layout_cornerse   = themes_path .. "zenburn/layouts/cornerse.png" --
-- }}} --

-- {{{ Titlebar --
--theme.titlebar_close_button_focus  = themes_path .. "zenburn/titlebar/close_focus.png" --
--theme.titlebar_close_button_normal = themes_path .. "zenburn/titlebar/close_normal.png" --

--theme.titlebar_minimize_button_normal = themes_path .. "default/titlebar/minimize_normal.png" --
--theme.titlebar_minimize_button_focus  = themes_path .. "default/titlebar/minimize_focus.png" --

--theme.titlebar_ontop_button_focus_active  = themes_path .. "zenburn/titlebar/ontop_focus_active.png" --
--theme.titlebar_ontop_button_normal_active = themes_path .. "zenburn/titlebar/ontop_normal_active.png" --
--theme.titlebar_ontop_button_focus_inactive  = themes_path .. "zenburn/titlebar/ontop_focus_inactive.png" --
--theme.titlebar_ontop_button_normal_inactive = themes_path .. "zenburn/titlebar/ontop_normal_inactive.png" --

--theme.titlebar_sticky_button_focus_active  = themes_path .. "zenburn/titlebar/sticky_focus_active.png" --
--theme.titlebar_sticky_button_normal_active = themes_path .. "zenburn/titlebar/sticky_normal_active.png" --
--theme.titlebar_sticky_button_focus_inactive  = themes_path .. "zenburn/titlebar/sticky_focus_inactive.png" --
--theme.titlebar_sticky_button_normal_inactive = themes_path .. "zenburn/titlebar/sticky_normal_inactive.png" --

--theme.titlebar_floating_button_focus_active  = themes_path .. "zenburn/titlebar/floating_focus_active.png"
--theme.titlebar_floating_button_normal_active = themes_path .. "zenburn/titlebar/floating_normal_active.png"
--theme.titlebar_floating_button_focus_inactive  = themes_path .. "zenburn/titlebar/floating_focus_inactive.png"
--theme.titlebar_floating_button_normal_inactive = themes_path .. "zenburn/titlebar/floating_normal_inactive.png"

--theme.titlebar_maximized_button_focus_active  = themes_path .. "zenburn/titlebar/maximized_focus_active.png"
--theme.titlebar_maximized_button_normal_active = themes_path .. "zenburn/titlebar/maximized_normal_active.png"
--theme.titlebar_maximized_button_focus_inactive  = themes_path .. "zenburn/titlebar/maximized_focus_inactive.png"
--theme.titlebar_maximized_button_normal_inactive = themes_path .. "zenburn/titlebar/maximized_normal_inactive.png"
-- }}} --
-- }}} --

-- {{ --
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
-- }} --

return theme
